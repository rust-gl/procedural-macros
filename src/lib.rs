extern crate proc_macro;

mod attribute;
mod uniform;

use ::syn::{GenericParam, Generics, TypeParamBound};
fn add_trait_bounds(mut generics: Generics, params: Vec<TypeParamBound>) -> Generics {
    for param in &mut generics.params {
        if let GenericParam::Type(ref mut type_param) = *param {
            for param in &params{
                type_param.bounds.push(param.clone());
            }
        }
    }
    generics
}
use ::proc_macro::TokenStream;

///Create a ToUniform implementaion from the given struct.
#[proc_macro_derive(ToUniform)]
pub fn gl_to_uniform(input: TokenStream) -> TokenStream {
    uniform::gl_to_uniform(input)
}
///Create a ToAttribute implementaion from the given struct.
#[proc_macro_derive(ToAttribute)]
pub fn gl_to_attribute(input: TokenStream) -> TokenStream {
    attribute::gl_to_attribute(input)
}
